package com.lrodlor.customTimerView;

import java.util.ArrayList;
import java.util.List;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Scroller;

import com.android.camera2.R;

public class CustomTimerView extends ViewGroup {

	private List<Item> mData = new ArrayList<Item>();
	
	public interface OnTimerTickListener {
		
		void OnTimerTick(CustomTimerView source);
	}
	
	public interface OnTimerSettedListener {
		
		void OnTimerSetted(CustomTimerView source, long millisToFire);
	}
	
	public interface OnTimerFinishedListener {
		
		void OnTimerFinished(CustomTimerView source);
	}
	
	private RectF customTimerViewBounds = new RectF();
	private RectF imageViewBounds = new RectF();
	
	private Paint normalPaint;
	private Paint highLightedPaint;
	private Paint pathPaint;
	
	private OnTimerFinishedListener onTimerFinishedListener = null;
	private OnTimerSettedListener onTimerSettedListener = null;
	private OnTimerTickListener onTimerTickListener = null;
	
	private Scroller mScroller;
	private ValueAnimator mScrollAnimator;
	private GestureDetector mDetector;
	private ImageView backImageView;
	private TimerView timerView;
	private Path mPath;
	
	//DENSITY DEPENDANT
	private int pathArcRadius;
	private int textSize;
	private int correctionX;
	private float correctionY;
	private float pathFactor;
	
	private int normalColor;
	private int highlightedColor;
	
	private int totalSeconds;
	
	private int mInterval = 30;
	private Handler mHandler;
	private long milliSecondsToFire = 0;
	
	private boolean longPressing = false;
	
	private int TOTAL_STICKS = 60;
	
	private CountDownTimer countDownTimer;
	
	public CustomTimerView(Context context) {
		
		super(context);
		init();
	}
	
	public CustomTimerView(Context context, AttributeSet attrs) {
		
		super(context, attrs);
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomTimerView, 0, 0);
		try {
			
			normalColor = a.getColor(R.styleable.CustomTimerView_normalColor, 0xff888888);
			highlightedColor = a.getColor(R.styleable.CustomTimerView_highlightedColor, 0xffffff00);
			totalSeconds = a.getInt(R.styleable.CustomTimerView_totalSeconds, 20);
			TOTAL_STICKS = a.getInt(R.styleable.CustomTimerView_totalSticks, 50);
			mInterval = a.getInt(R.styleable.CustomTimerView_timeInterval, 30);
		} finally {

			a.recycle();
		}
		init();
	}
	
	public CustomTimerView(Context context, AttributeSet attrs, int defStyleAttr) {
		
		super(context, attrs, defStyleAttr);
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomTimerView, 0, 0);
		try {
			
			normalColor = a.getColor(R.styleable.CustomTimerView_normalColor, 0xff888888);
			highlightedColor = a.getColor(R.styleable.CustomTimerView_highlightedColor, 0xffffff00);
			totalSeconds = a.getInt(R.styleable.CustomTimerView_totalSeconds, 20);
			TOTAL_STICKS = a.getInt(R.styleable.CustomTimerView_totalSticks, 50);
			mInterval = a.getInt(R.styleable.CustomTimerView_timeInterval, 30);
		} finally {

			a.recycle();
		}
		init();
	}
	
	public int getNormalColor() {
		
		return this.normalColor;
	}
	
	public void setNormalColor (int color) {
		
		this.normalColor = color;
	}
	
	public int getHighLightedColor() {
		
		return this.highlightedColor;
	}
	
	public void setHighLightedColor(int color) {
		
		this.highlightedColor = color;
	}
	
	public int getTotalSeconds() {
		
		return this.totalSeconds;
	}
	
	public void setTotalSeconds(int second) {
		
		this.totalSeconds = second;
	}
	
	public void setOnTickListener(OnTimerTickListener listener) {
		
		this.onTimerTickListener = listener;
	}
	
	public void setOnTimerSettedListener(OnTimerSettedListener listener) {
		
		this.onTimerSettedListener = listener;
	}
	
	public void setOnTimerFinishedListener(OnTimerFinishedListener listener) {
		
		this.onTimerFinishedListener = listener;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		boolean result = mDetector.onTouchEvent(event);
		
		if (event.getAction() == MotionEvent.ACTION_UP) {
			
			if(longPressing) {
				
				if (onTimerSettedListener!=null)
					onTimerSettedListener.OnTimerSetted(CustomTimerView.this,milliSecondsToFire);
				
				startCountDownTimer();
			}
			
			mHandler.removeCallbacks(mStatusChecker);
			longPressing = false;
		}
		if (longPressing && countDownTimer==null) {
			
			mStatusChecker.run();
		}
		else if (longPressing && countDownTimer!=null) {
			
			countDownTimer.cancel();
			countDownTimer = null;
			mStatusChecker.run();
		}
		
		return result;
	}
	
	protected void startCountDownTimer() {
		
		countDownTimer = new CountDownTimer(milliSecondsToFire, 100) {

		     public void onTick(long millisUntilFinished) {
		    	 
		    	 milliSecondsToFire = millisUntilFinished;
		    	 if (onTimerTickListener!=null)
		    		 onTimerTickListener.OnTimerTick(CustomTimerView.this);
		         onDataChanged();
		     }

		     public void onFinish() {
		    	 
		    	 if (onTimerFinishedListener!=null)
		    		 onTimerFinishedListener.OnTimerFinished(CustomTimerView.this);
		    	 
		    	 milliSecondsToFire = 0;
		    	 timerView.invalidate();
		    	 backImageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.timer_unselected));
		    	 countDownTimer = null;
		     }
		  };
		  countDownTimer.start();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		
		super.onDraw(canvas);
	}
	
	@Override
	protected int getSuggestedMinimumWidth() {
		
		return 100;
	}

	@Override
	protected int getSuggestedMinimumHeight() {
		
		return 100;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
		int w = Math.max(minw, MeasureSpec.getSize(widthMeasureSpec));

		int h = w;

		setMeasuredDimension(w, h);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		
		super.onSizeChanged(w, h, oldw, oldh);
		
		float xpad = (float) (getPaddingLeft() + getPaddingRight());
		float ypad = (float) (getPaddingTop() + getPaddingBottom());


		float ww = (float) w - xpad;
		float hh = (float) h - ypad;
		
		float diameter = Math.min(ww, hh);
		
		customTimerViewBounds = new RectF(xpad/2, ypad/2, diameter+xpad/2, diameter+ypad/2);

		imageViewBounds = new RectF(xpad/2+diameter/2-(2*diameter/7),ypad/2+diameter/2-(2*diameter/7),xpad/2+diameter/2+(2*diameter/7),ypad/2+diameter/2+(2*diameter/7));
		backImageView.layout((int)imageViewBounds.left, (int)imageViewBounds.top, (int)imageViewBounds.right, (int)imageViewBounds.bottom);
		
		timerView.layout((int) customTimerViewBounds.left, (int) customTimerViewBounds.top, (int) (customTimerViewBounds.right), (int) (customTimerViewBounds.bottom));
		timerView.setPivot(customTimerViewBounds.width()/2, customTimerViewBounds.height() / 2+ypad/2);
		
		mPath = new Path();
		mPath.moveTo(imageViewBounds.left+pathFactor, imageViewBounds.top+(imageViewBounds.bottom-imageViewBounds.top)/2+pathArcRadius-ypad/2);
		mPath.lineTo(pathFactor,imageViewBounds.top+(imageViewBounds.bottom-imageViewBounds.top)/2+pathArcRadius);
		
		final RectF oval = new RectF();

		oval.set(0, ypad/2+diameter/2 - pathArcRadius-ypad/2, 2*pathArcRadius, ypad/2+diameter/2+pathArcRadius-ypad/2);
		
		mPath.addArc(oval, 90, 180);
		mPath.lineTo(imageViewBounds.left+pathFactor, imageViewBounds.top+(imageViewBounds.bottom-imageViewBounds.top)/2-pathArcRadius-ypad/2);
		mPath.lineTo(imageViewBounds.left+pathFactor, imageViewBounds.top+(imageViewBounds.bottom-imageViewBounds.top)/2+pathArcRadius-ypad/2);
		mPath.close();
		
		onDataChanged();
	}
	
	private void onDataChanged() {

		timerView.accelerate();
		int milliSecondsPerStick = totalSeconds*1000/TOTAL_STICKS;
		long highlightedSticks = milliSecondsToFire/milliSecondsPerStick;
		
		int aux=0;
		for (Item it : mData) {
				
			if (aux<highlightedSticks)
				it.highlighted = true;
			else
				it.highlighted = false;
			aux++;
		}
		timerView.invalidate();
		onScrollFinished();
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {}
	
	
	private void init() {
		
		normalPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		normalPaint.setStyle(Paint.Style.FILL);
		normalPaint.setColor(normalColor);
		setLayerToSW(this);

		// Set up the paint for the pie slices
		highLightedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		highLightedPaint.setStyle(Paint.Style.FILL);
		highLightedPaint.setColor(highlightedColor);
		
		pathPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		pathPaint.setStyle(Paint.Style.FILL);
		pathPaint.setColor(0x59000000);
		
		if (timerView == null) {
			
			timerView = new TimerView(getContext());
			addView(timerView);
		}

		backImageView = new ImageView(getContext());
		backImageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.timer_unselected));
		addView(backImageView);
		
		mScroller = new Scroller(getContext(), null, true);
		mScrollAnimator = ValueAnimator.ofFloat(0, 1);
		mScrollAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				
				tickScrollAnimation();
			}
		});

		mDetector = new GestureDetector(CustomTimerView.this.getContext(), new GestureListener());
		mDetector.setIsLongpressEnabled(true);

		for (int i=0;i<TOTAL_STICKS;i++) {
			
			Item it = new Item();
			mData.add(it);
		}
		
		mHandler = new Handler();
		
		int density = (int) getResources().getDisplayMetrics().density;
		
		if (density >= 4.0) { //"xxxhdpi";
			
			pathFactor = 133.3f;
			correctionX = 51;
			correctionY = 6.7f;
			textSize = 60;
			pathArcRadius = 67;
		}
		else if (density >= 3.0 && density < 4.0) { //xxhdpi
			
			pathFactor = 100.0f;
			correctionX = 40;
			correctionY = 5.0f;
			textSize = 47;
			pathArcRadius = 50;
		}
		else if (density >= 2.0) { //xhdpi
			
			pathFactor = 66.6f;
			correctionX = 22;
			correctionY = 4.3f;
			textSize = 35;
			pathArcRadius = 33;
		}
		else if (density >= 1.5 && density < 2.0) { //hdpi
			
			pathFactor = 50.0f;
			correctionX = 15;
			correctionY = 4f;
			textSize = 27;
			pathArcRadius = 25;
		}
		else if (density >= 1.0 && density < 1.5) { //mdpi
			
			pathFactor = 33.3f;
			correctionX = 10;
			correctionY = 4f;
			textSize = 20;
			pathArcRadius = 17;
		}
		
		highLightedPaint.setTextAlign(Align.CENTER);
		highLightedPaint.setTextSize(textSize);
		normalPaint.setTextAlign(Align.CENTER);
		normalPaint.setTextSize(textSize);
	}
	
	private Runnable mStatusChecker = new Runnable() {
		
	    @Override 
	    public void run() {
	    	
	    	if (milliSecondsToFire<=totalSeconds*1000)
	    		milliSecondsToFire += 100;
	    	onDataChanged();
	    	mHandler.postDelayed(mStatusChecker, mInterval);
	    }
	  };

	private void tickScrollAnimation() {
		
		if (!mScroller.isFinished()) {
			
			mScroller.computeScrollOffset();
			//setTimerRotation(mScroller.getCurrY());
		} else {
			
			mScrollAnimator.cancel();
			onScrollFinished();
		}
	}

	private void setLayerToSW(View v) {
		
		if (!v.isInEditMode()) {
			
			setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}
	}

	private void setLayerToHW(View v) {
		
		if (!v.isInEditMode()) {
			
			setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}
	}

	/**
	 * Called when the user finishes a scroll action.
	 */
	private void onScrollFinished() {
		
		timerView.decelerate();
	}

	/**
	 * Internal child class that draws the pie chart onto a separate hardware
	 * layer when necessary.
	 */
	private class TimerView extends View {

		private PointF mPivot = new PointF();
		
		private Bitmap barBitmap;
		private Bitmap highlightedBarBitmap;

		/**
		 * Construct a PieView
		 *
		 * @param context
		 */
		public TimerView(Context context) {
			super(context);
		}

		/**
		 * Enable hardware acceleration (consumes memory)
		 */
		public void accelerate() {
			setLayerToHW(this);
		}

		/**
		 * Disable hardware acceleration (releases memory)
		 */
		public void decelerate() {
			setLayerToSW(this);
		}

		@Override
		protected void onDraw(Canvas canvas) {
			
			super.onDraw(canvas);
			normalPaint.setXfermode(new PorterDuffXfermode(Mode.ADD));
			
			if (barBitmap==null) {
				
				Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bar, null);
				barBitmap = Bitmap.createScaledBitmap(bitmap, 2*getWidth()/5, bitmap.getHeight(), true);
			}
			if (highlightedBarBitmap==null) {
				
				Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.lightbar, null);
				highlightedBarBitmap = Bitmap.createScaledBitmap(bitmap, 2*getWidth()/5, bitmap.getHeight(), true);
			}
			
			
			for (int i = 0; i< mData.size(); i++) {

				Item it = mData.get(i);
				
				Matrix matrix = new Matrix();
			    matrix.postRotate(i*360/TOTAL_STICKS+180);
			    matrix.postTranslate(getWidth()/2, getHeight()/2);
			    
			    if (!it.highlighted)
			    	canvas.drawBitmap(barBitmap, matrix, normalPaint);
			    else
			    	canvas.drawBitmap(highlightedBarBitmap, matrix, normalPaint);
			}
			
			normalPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
			
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inMutable = true;
			options.inPreferredConfig = Bitmap.Config.ALPHA_8;
			
		    canvas.drawPath(mPath, normalPaint);
		    canvas.drawCircle(getWidth()/2, getHeight()/2, 2*getWidth()/7, normalPaint);
			
			canvas.drawPath(mPath, pathPaint);
			
			normalPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
			canvas.drawCircle(getWidth()/2, getHeight()/2, 2*getWidth()/7, normalPaint);
			
			Rect areaRect = new Rect(0, canvas.getHeight()/2-pathArcRadius, 4*getWidth()/14+correctionX, canvas.getHeight()/2+pathArcRadius);
			RectF bounds = new RectF(areaRect);
			bounds.right = highLightedPaint.measureText(""+((int)milliSecondsToFire/1000), 0, (""+((int)milliSecondsToFire/1000)).length());
			bounds.bottom = - highLightedPaint.ascent();

			bounds.left += (areaRect.width() - bounds.right) / 2.0f;
			bounds.top += (areaRect.height() - bounds.bottom) / 2.0f+correctionY;
			
			if (milliSecondsToFire>0)
				canvas.drawText(""+((int)milliSecondsToFire/1000), bounds.left, bounds.top, highLightedPaint);
			else
				canvas.drawText(""+((int)milliSecondsToFire/1000), bounds.left, bounds.top, normalPaint);
		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {}

		public void setPivot(float x, float y) {
			
			mPivot.x = x;
			mPivot.y = y;
			
			setPivotX(x);
			setPivotY(y);
		}
	}
	
	/**
	 * Extends {@link GestureDetector.SimpleOnGestureListener} to provide custom
	 * gesture processing.
	 */
	private class GestureListener extends GestureDetector.SimpleOnGestureListener {
		
		@Override
		public void onLongPress(MotionEvent e) {
			//Swapped to ondown.
		
		}
		
		@Override
		public boolean onDown(MotionEvent e) {
			longPressing = true;
			backImageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.timer_selected));
			return true;
		}
	}
	
	private class Item {
		
		public boolean highlighted;
		
		public Item() {
			
			super();
		}
	}
}